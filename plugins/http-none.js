import axios from 'axios'

const http = axios.create({
    baseURL: 'http://localhost:8080'
})

http.interceptors.response.use(function (response) {
    console.log('huybo una respuesta', response)
    return response
}, function (error) {
    if (error.response.status === 480) {
        console.log('el token no es valido')
    } else {
        console.log('hubo un error en respuesta', error)
        return Promise.reject(error)
    }
})

export default http
