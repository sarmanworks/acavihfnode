import createPersistedState from 'vuex-persistedstate'

export default ({ store, $axios }) => {
    createPersistedState({
        key: 'acavih',
        paths: ['auth']
    })(store)
    console.log(store.state.auth)
    $axios.defaults.headers.authorization = store.state.auth.token

    window.console.error = (e) => {
        store.commit('sendError', e)
        console.log('Ha sucedido un error', e)
    }
}
