const Partner = require('./models/Members')
const { connectDB } = require('./utils/connect-db')

connectDB().then(async () => {
    console.log('Conectado correctamente')

    const members = await Partner.find()
    const qmembers = members.filter((m) => {
        return `${m.nombre} ${m.apellidos} ${m.codigo}`
            .toLowerCase().match(new RegExp('Stab.*694485428', 'gi'))
    })
    console.log(qmembers)
})
