require('dotenv/config')
const http = require('http')
const { connectDB } = require('./utils/connect-db')
const { PORT } = require('./vars')

async function main () {
    try {
        await connectDB()
        const server = http.createServer(require('./app'))
        server.listen(PORT, () => {
            console.log('Escuchando por el puerto ' + PORT)
        })
    } catch (err) {
        console.log(err)
        process.exit(1)
    }
}

main()
