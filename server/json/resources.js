const { getResoruce } = require('../models/Resource')

/**
 * @type {import('mongoose').Model}
 */
const Nacionalidad = getResoruce('nacionalidad').model
/**
 * @type {import('mongoose').Model}
 */
const Residencia = getResoruce('residencia').model
/**
 * @type {import('mongoose').Model}
 */
const Sexo = getResoruce('sexo').model
/**
* @type {import('mongoose').Model}
*/
const Proyecto = getResoruce('proyectos').model
/**
 * @type {import('mongoose').Model}
 */
const Motivo = getResoruce('motivosatencion').model

/**
 * @type {import('mongoose').Model}
 */
const Derivacion = getResoruce('derivaciones').model

/**
 * @type {import('mongoose').Model}
 */
const Socio = getResoruce('socioono').model

/**
 * @type {import('mongoose').Model}
 */
const TipoAtenciones = getResoruce('tipoatenciones').model

module.exports = { Nacionalidad, Residencia, Sexo, Proyecto, Motivo, Derivacion, Socio, TipoAtenciones }
