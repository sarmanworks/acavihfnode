const _ = require('lodash')
const proyectos = require('./proyectos.json')
const derivados = require('./derivacion.json')
const motivos = require('./motivos.json')
const tipoAtencionesJson = require('./tipoatenciones.json')

// const motivos = require('./moti')

const miembros = require('./members.json').map((m) => {
    return {
        id: m.id,
        codigo: m.codigo,
        nombre: m.nombre,
        apellidos: m.surname,
        sexo: m.sexo,
        sipcard: m.sipcard,
        correoelectronico: m.email,
        phone: m.phone,
        observaciones: m.observaciones,
        cosaspendientes: m.cosaspendientes,
        socioono: m.essocio,
        nacionalidad: m.nacionalidad,
        ciudadresidencia: m.ciudadderesidencia,
        fechanacimiento: new Date(m.fechanacimiento).getTime()
    }
})

const atenciones = require('./atenciones.json').map((a) => {
    const data = {
        comentario: a.comentario,
        user: a.member,
        fechaatencion: a.fecha_atencion,
        Proyectos: a.proyectos || [],
        derivadoa: a.derivado__a || [],
        derivadode: a.derivado_de || [],
        motivosatencion: a.nombre_motivo || [],
        cosaspendientes: a.cosas_pendientes,
        tipoatenciones: a.tipo_atenciones || []
    }
    data.Proyectos = data.Proyectos.map(p => proyectos[p - 1].nombre_proyecto)
    data.derivadoa = data.derivadoa.map(d =>
        derivados[d - 1] ? derivados[d - 1].name : null
    ).filter(a => a != null)
    data.derivadode = data.derivadode.map(d =>
        derivados[d - 1] ? derivados[d - 1].name : null
    ).filter(a => a != null)
    data.motivosatencion = data.motivosatencion.map(e => motivos[e - 1].nombre_motivo)
    data.tipoatenciones = data.tipoatenciones.map(e => tipoAtencionesJson[e - 1].nombre_atencion)
    return data
})

const nacionalidades = removeDuplicates(miembros.map(m => m.nacionalidad))
const residencias = removeDuplicates(miembros.map(m => m.ciudadresidencia))
const socios = removeDuplicates(miembros.map(m => m.socioono))
const sexos = removeDuplicates(miembros.map(m => m.sexo))

const proyectosComputed = removeDuplicates(_.flatten(atenciones.map(a => a.Proyectos)))
const motivosatencion = removeDuplicates(_.flatten(atenciones.map(a => a.motivosatencion)))
const tipoAtenciones = removeDuplicates(_.flatten(atenciones.map(a => a.tipoatenciones)))
const derivaciones = removeDuplicates(_.flattenDeep([
    atenciones.map(a => a.derivadoa),
    atenciones.map(a => a.derivadode)
]))

module.exports = {
    data: { miembros, atenciones },
    items: { nacionalidades, socios, residencias, sexos, proyectosComputed, motivosatencion, derivaciones, tipoAtenciones }
}

function removeDuplicates (elements) {
    return _.uniq(elements).filter(e => e != null)
}
