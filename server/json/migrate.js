const { connectDB } = require('../utils/connect-db')
const Partner = require('../models/Members')
const Atencion = require('../models/Atencion')
const { Nacionalidad, Residencia, Sexo, Proyecto, Motivo, Derivacion, Socio, TipoAtenciones } = require('./resources')
const data = require('./sanitizeddata')

connectDB().then(async () => {
    // const atenciones = await Atencion.find({}).populate('tipoatenciones')
    // console.log(atenciones.filter(a => a.tipoaenciones.length > 0))
    // console.log(data.items.tipoAtenciones)
    await migrateData()
    /* const items = await Atencion.find({}).limit(10).populate('Proyectos').populate('derivadoa')
    console.log(items[8]); */
}).catch((err) => {
    console.log(err)
})

async function migrateData () {
    const getIdArray = (array, name) => {
        try {
            if (name == null) { return null }
            return array.filter(a => a.name === name)[0]._id
        } catch (error) {
            console.log(name)
            return name
        }
    }

    const partners = []

    const nacionalidades = await putItems(Nacionalidad, data.items.nacionalidades)
    const residencias = await putItems(Residencia, data.items.residencias)
    const socios = await putItems(Socio, data.items.socios)
    const sexos = await putItems(Sexo, data.items.sexos)
    const proyectos = await putItems(Proyecto, data.items.proyectosComputed)
    const motivos = await putItems(Motivo, data.items.motivosatencion)
    const derivaciones = await putItems(Derivacion, data.items.derivaciones)
    const tipoAtenciones = await putItems(TipoAtenciones, data.items.tipoAtenciones)

    for (let index = 0; index < data.data.miembros.length; index++) {
        const member = data.data.miembros[index]
        member.sexo = getIdArray(sexos, member.sexo)
        member.socioono = getIdArray(socios, member.socioono)
        member.nacionalidad = getIdArray(nacionalidades, member.nacionalidad)
        member.ciudadresidencia = getIdArray(residencias, member.ciudadresidencia)
        console.log(member)
        const memberDB = JSON.parse(JSON.stringify(await Partner.create(member)))
        memberDB.idsql = member.id
        partners.push(memberDB)
    }

    for (let index = 0; index < data.data.atenciones.length; index++) {
        const atencion = data.data.atenciones[index]
        atencion.fechaatencion = new Date(atencion.fechaatencion).getTime()
        atencion.Proyectos = atencion.Proyectos.map(p => getIdArray(proyectos, p))
        atencion.derivadoa = atencion.derivadoa.map(p => getIdArray(derivaciones, p))
        atencion.derivadode = atencion.derivadode.map(p => getIdArray(derivaciones, p))
        atencion.motivosatencion = atencion.motivosatencion.map(p => getIdArray(motivos, p))
        atencion.tipoaenciones = atencion.tipoatenciones.map(p => getIdArray(tipoAtenciones, p))
        atencion.user = partners.filter(a => a.idsql === atencion.user)[0]._id
        await Atencion.create(atencion)
    }

    console.log('Migracion hecha')

    // console.log(getIdArray(nacionalidades, 'Uruguaya'))
}

/**
 *
 * @param {import('mongoose').Model} Model
 * @param {String} items
 */
async function putItems (Model, items) {
    const itemsDB = []
    for (let index = 0; index < items.length; index++) {
        const item = items[index]
        const itemDB = await Model.create({ name: item })
        itemsDB.push(itemDB)
    }
    return itemsDB
}
