const express = require('express')

const app = express()
const cors = require('cors')

app.use(cors())

app.get('/', function (req, res) {
    res.end('Hola mundo')
})

app.use(require('./routes'))

module.exports = app
