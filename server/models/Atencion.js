const mongoose = require('mongoose')

const atencion = new mongoose.Schema({
    comentario: { type: String },
    fechaatencion: { type: String },
    tipoaenciones: [{ type: mongoose.Types.ObjectId, ref: 'tipoatenciones' }],
    Proyectos: [{ type: mongoose.Types.ObjectId, ref: 'proyectos' }],
    motivosatencion: [{ type: mongoose.Types.ObjectId, ref: 'motivosatencion' }],
    derivadoa: [{ type: mongoose.Types.ObjectId, ref: 'derivaciones' }],
    derivadode: [{ type: mongoose.Types.ObjectId, ref: 'derivaciones' }],
    formacion: [{ type: mongoose.Types.ObjectId, ref: 'formacion' }],
    voluntariado: [{ type: mongoose.Types.ObjectId, ref: 'voluntariado' }],
    cosaspendientes: { type: String },
    user: { type: mongoose.Types.ObjectId, ref: 'partners' }
})

const Atencion = mongoose.model('atencion', atencion)

module.exports = Atencion
