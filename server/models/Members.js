const mongoose = require('mongoose')

const partner = new mongoose.Schema({
    codigo: { type: String },
    nombre: { type: String },
    edad: { type: Number },
    apellidos: { type: String },
    fechanacimiento: { type: Number },
    sipcard: { type: String },
    correoelectronico: { type: String },
    telefono: { type: String },
    observaciones: { type: String },
    cosaspendientes: { type: String },
    sexo: { type: mongoose.Types.ObjectId, ref: 'sexo' },
    socioono: { type: mongoose.Types.ObjectId, ref: 'socioono' },
    nacionalidad: { type: mongoose.Types.ObjectId, ref: 'nacionalidad' },
    ciudadresidencia: { type: mongoose.Types.ObjectId, ref: 'residencia' }
}, { toJSON: { virtuals: true } })

partner.virtual('fullname')
    .get(() => this)

const Partner = mongoose.model('partners', partner)

module.exports = Partner
