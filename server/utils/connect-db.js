const mongoose = require('mongoose')
const { DB_URI } = require('../vars')

/**
 * @type {import('mongoose').ConnectionOptions}
 */
const mongooseOptions = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: true
}

module.exports.connectDB = () => new Promise((resolve, reject) => {
    mongoose.connect(DB_URI, mongooseOptions, (err) => {
        if (err) {
            console.log(err)
            console.log('No se pudo conectar a la base de datos ' + DB_URI)
            reject(err)
        }
        console.log('Se ha podido conectar a la base datos correctamente ' + DB_URI)
        resolve()
    })
})
