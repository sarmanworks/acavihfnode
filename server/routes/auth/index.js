const express = require('express')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const User = require('../../models/User')
const { SALTS, TOKEN_KEY } = require('../../vars')

const router = express.Router()

router.post('/register', register)
router.post('/login', login)

module.exports = router

/**
 *
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 * @returns
 */
async function login (req, res) {
    // Buscar el usuario
    const user = await User.findOne({ user: req.body.user })
    console.log(user)
    if (user == null) {
        return res.status(404).end('El usuario no se encontro')
    }
    // Comprobar la contraseña
    console.log(user.password, req.body.password)
    if (!await bcrypt.compare(req.body.password, user.password)) {
        return res.status(403).end('La contraseña no es correcta')
    }
    // Crear el token
    const token = jwt.sign({ user: user.user }, TOKEN_KEY, { expiresIn: '6h' })
    res.setHeader('authorization', token)
    res.json({
        user, token
    })
    // Responderle al usuario
}

async function register (req, res) {
    // Guardar el usuario con la contraseña cifrada
    const user = await User.create({
        ...req.body,
        password: await bcrypt.hash(req.body.password, SALTS)
    })
    res.json({
        message: 'Usuario creado',
        user
    })
}
