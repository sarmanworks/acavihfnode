const express = require('express')
const Atencion = require('../../../models/Atencion')

const router = express.Router()

router.post('/', createAtencion)
router.get('/for/:memberId', getAttentionsFor)
router.get('/show/:id', getAttention)
router.put('/edit/:id', updateAtention)

module.exports = router

async function createAtencion (req, res) {
    const data = await Atencion.create(req.body)
    res.json(data)
}

async function updateAtention (req, res) {
    await Atencion.findOneAndUpdate({ _id: req.params.id }, { $set: req.body })
    res.json(await Atencion.findOne({ _id: req.params.id }))
}

async function getAttentionsFor (req, res) {
    const data = await Atencion.find({ user: req.params.memberId }).sort([['fechaatencion', -1]])
        .populate('tipoaenciones')
        .populate('Proyectos')
        .populate('motivosatencion')
        .populate('derivadoa')
        .populate('derivadode')
        .populate('formacion')
        .populate('voluntariado')
    res.json(data)
}

async function getAttention (req, res) {
    const data = await Atencion.findOne({ _id: req.params.id })
        .populate('tipoaenciones')
        .populate('derivadoa')
        .populate('derivadode')
        .populate('Proyectos')
        .populate('motivosatencion')
        .populate('formacion')
        .populate('voluntariado')
    res.json(data)
}
