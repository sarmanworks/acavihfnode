const jwt = require('jsonwebtoken')
const User = require('../../models/User')
const { TOKEN_KEY } = require('../../vars')

module.exports.authorize = async (req, res, next) => {
    const auth = req.headers.authorization
    if (!auth) {
        return res.status(403).json('No tienes permitido acceder aquí')
    }
    try {
        const token = jwt.verify(auth, TOKEN_KEY)
        const user = await User.findOne({ user: token.user })
        if (!user) {
            return res.status(480).end('El token no es válido')
        }
        next()
    } catch {
        res.status(401).end('Su sesión no es válida')
    }
}
