const express = require('express')
const Member = require('../../../models/Members')

const DEFAULT_SIZE_PAGE = 10

const router = express.Router()

router.post('/', create)
router.get('/', index)
router.get('/query', showMember)
router.get('/:id', showMember)
router.put('/edit/:id', update)

module.exports = router

async function create (req, res) {
    res.json(await Member.create(req.body))
}

async function index (req, res) {
    const data = await Member.find({})
        .collation({ locale: 'en' })
        .populate('sexo')
        .populate('socioono')
        .populate('nacionalidad')
        .populate('ciudadresidencia')
        .sort([['nombre', 1]])
        .limit(DEFAULT_SIZE_PAGE)
        .skip(DEFAULT_SIZE_PAGE * (req.query.page - 1) || 0)
    res.json(data)
}

async function showMember (req, res) {
    let data
    if (req.params.id) {
        const query = { _id: req.params.id }
        data = await Member.findOne(query)
            .populate('sexo')
            .populate('socioono')
            .populate('nacionalidad')
            .populate('ciudadresidencia')
    } else {
        const _data = await Member.find({})
            .populate('sexo')
            .populate('socioono')
            .populate('nacionalidad')
            .populate('ciudadresidencia')
        data = _data.filter((m) => {
            return `${m.nombre} ${m.apellidos} ${m.codigo} ${m.correoelectronico} ${m.sipcard}`
                .toLowerCase().match(new RegExp(req.query.q.replace(/\s/g, '.*'), 'gi')) // eslint-disable-line
        })
    }
    res.json(data)
}

async function update (req, res) {
    await Member.findOneAndUpdate({ _id: req.params.id }, { $set: req.body })
    res.json(await Member.findOne({ _id: req.params.id }))
}
