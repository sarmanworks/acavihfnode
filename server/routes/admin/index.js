const express = require('express')
const { authorize } = require('./authorize')

const router = express.Router()

router.use(authorize)

router.use('/resources', require('./resources'))
router.use('/members', require('./members'))
router.use('/atenciones', require('./atenciones'))

router.get('/', (req, res) => {
    res.end('Esta es la zona privada')
})

module.exports = router
