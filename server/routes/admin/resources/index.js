const express = require('express')
const { resources } = require('../../../models/Resource')
const { getRouterFor } = require('./get-router-for')

const router = express.Router()

for (let index = 0; index < resources.length; index++) {
    const resource = resources[index]
    router.use('/' + resource.name, getRouterFor(resource.name, resource.model))
}

module.exports = router
