const express = require('express')
// eslint-disable-next-line no-unused-vars
const { Model } = require('mongoose')

/**
 *
 * @param {String} name
 * @param {Model} model
 * @returns
 */
function getRouterFor (name, model) {
    const router = express.Router()

    router.get('/', async (req, res) => res.json(await model.find({})))
    router.post('/', async function (req, res) {
        res.json(await model.create(req.body))
    })
    router.put('/edit/:id', async function (req, res) {
        await model.findOneAndUpdate({ _id: req.params.id }, {
            $set: req.body
        })
        return res.json(await model.findOne({ _id: req.params.id }))
    })

    return router
}

module.exports.getRouterFor = getRouterFor
