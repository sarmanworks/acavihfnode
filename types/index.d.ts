declare module '@nuxt/vue-app' {
  interface Context {
    $http: NuxtAxiosInstance
  }
  interface NuxtAppOptions {
    $http: NuxtAxiosInstance
  }
}