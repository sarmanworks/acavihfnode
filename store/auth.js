import { getField, updateField } from 'vuex-map-fields'

export const state = () => ({
    token: ''
})

export const mutations = {
    updateField,
    setToken (state, token) {
        state.token = token
    }
}

export const getters = {
    getField,
    token (state) {
        return state.token
    }
}
