export const state = () => ({
    errors: []
})

export const mutations = {
    sendError (state, error) {
        state.errors.push(error)
    }
}
